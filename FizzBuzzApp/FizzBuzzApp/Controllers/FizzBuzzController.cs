﻿using FizzBuzzApp.Models;
using FizzBuzzServices.Interfaces;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FizzBuzzApp.Controllers
{
    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzService _fizzBuzzService;
        private const int PageSize = 20;

        public FizzBuzzController(IFizzBuzzService fizzBuzzService)
        {
            _fizzBuzzService = fizzBuzzService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            ModelState.Clear();
            var emptyModel = new FizzBuzzModel() { FizzBuzzList = new PagedList<string>(new List<string>(), 1, PageSize) };
            return View("Index", emptyModel);
        }

        [HttpGet]
        public ActionResult GetList(FizzBuzzModel fizzBuzzModel)
        {
            if (ModelState.IsValid)
            {
                var fizzbuzzList = _fizzBuzzService.GetFizzBuzzList(fizzBuzzModel.Number??0);
                if ((fizzBuzzModel.Page ?? 1) * PageSize > fizzbuzzList.Count())
                {
                    fizzBuzzModel.Page = GetMaxPageNumber(fizzbuzzList.Count());
                }
                fizzBuzzModel.FizzBuzzList = fizzbuzzList.ToPagedList(fizzBuzzModel.Page ?? 1, PageSize);
            }
            return View("Index", fizzBuzzModel);
        }

        /// <summary>
        /// Method to get max page number of paged list
        /// </summary>
        /// <param name="totalItems">total items in list</param>
        /// <returns>max page number</returns>
        private int GetMaxPageNumber(double totalItems)
        {
            var mxPageSize = totalItems / PageSize;
            return (int)Math.Ceiling(mxPageSize);
        }
    }
}
