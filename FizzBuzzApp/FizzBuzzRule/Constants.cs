﻿
namespace FizzBuzzRule
{
    /// <summary>
    /// This contains all constants used in application
    /// </summary>
    public static class Constants
    {
        // constant for fizz string
        public const string Fizz = "fizz";

        // constant for buzz string
        public const string Buzz = "buzz";

        // constant for wizz string
        public const string Wizz = "wizz";

        // constant for wuzz string
        public const string Wuzz = "wuzz";
    }
}
