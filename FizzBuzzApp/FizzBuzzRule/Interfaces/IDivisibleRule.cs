﻿using System;
namespace FizzBuzzRule.Interfaces
{
    /// <summary>
    /// Interface for divisible rules
    /// </summary>
    public interface IDivisibleRule
    {
        /// <summary>
        /// Property to return result of rule
        /// </summary>
        string Result { get;}

        /// <summary>
        /// To check rule divisibility conditions 
        /// </summary>
        /// <param name="input">input number to check divisibility</param>
        /// <param name="dayOfTheWeek">day Of The Week</param>
        /// <returns>true if rule pass else return false</returns>
        bool Run(int input, DayOfWeek dayOfTheWeek);
    }
}
