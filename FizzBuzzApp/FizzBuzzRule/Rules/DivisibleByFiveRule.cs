﻿using FizzBuzzRule.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzRule.Rules
{
    /// <summary>
    /// Rule to check divisibility of 5
    /// </summary>
    public class DivisibleByFiveRule : IDivisibleRule
    {
        private string _result = string.Empty;

        /// <summary>
        /// Property to return result of rule
        /// </summary>
        public string Result
        {
            get { return _result; }
        }

        /// <summary>
        /// To check rule divisibility conditions by 5 
        /// </summary>
        /// <param name="input">input number to check divisibility</param>
        /// <param name="dayOfTheWeek">day Of The Week</param>
        /// <returns>true if rule pass else return false</returns>
        public bool Run(int input, DayOfWeek dayOfTheWeek)
        {
            if (input % 5 == 0)
            {
                _result = dayOfTheWeek == DayOfWeek.Wednesday ? Constants.Wuzz : Constants.Buzz;
                return true;
            }
            return false;
        }
    }
}
