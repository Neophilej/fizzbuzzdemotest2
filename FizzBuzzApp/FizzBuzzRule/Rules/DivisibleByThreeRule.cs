﻿using FizzBuzzRule.Interfaces;
using System;

namespace FizzBuzzRule.Rules
{
    /// <summary>
    /// Rule to check divisibility of 3
    /// </summary>
    public class DivisibleByThreeRule : IDivisibleRule
    {
        private string _result = string.Empty;

        /// <summary>
        /// Property to return result of rule
        /// </summary>
        public string Result
        {
            get { return _result; }
        }

        /// <summary>
        /// To check rule divisibility conditions by 3 
        /// </summary>
        /// <param name="input">input number to check divisibility</param>
        /// <param name="dayOfTheWeek">day Of The Week</param>
        /// <returns>true if rule pass else return false</returns>
        public bool Run(int input, DayOfWeek dayOfTheWeek)
        {
            if (input % 3 == 0)
            {
                _result = dayOfTheWeek == DayOfWeek.Wednesday ? Constants.Wizz : Constants.Fizz;
                return true;
            }
            return false;
        }
    }
}
