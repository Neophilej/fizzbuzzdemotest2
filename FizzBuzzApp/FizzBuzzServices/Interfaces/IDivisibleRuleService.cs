﻿using FizzBuzzRule.Interfaces;
using System.Collections.Generic;

namespace FizzBuzzServices.Interfaces
{
    /// <summary>
    /// Interface of DivisibleRuleService
    /// </summary>
    public interface IDivisibleRuleService
    {
        /// <summary>
        /// Function to get all rules which returned true for the given input
        /// </summary>
        /// <param name="number">input number</param>
        /// <returns>Enumerable of rules which returned true for the given input</returns>
        IEnumerable<IDivisibleRule> GetMatchedRules(int number);
    }
}
