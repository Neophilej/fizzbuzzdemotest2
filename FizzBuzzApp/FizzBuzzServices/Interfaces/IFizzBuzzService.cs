﻿using System.Collections.Generic;

namespace FizzBuzzServices.Interfaces
{
    /// <summary>
    /// Interface for fizz buzz service
    /// </summary>
    public interface IFizzBuzzService
    {
        /// <summary>
        ///  Method to get fizz buzz list
        /// </summary>
        /// <param name="maxLimit">input number</param>
        /// <returns>Fizz buzz list</returns>
        IEnumerable<string> GetFizzBuzzList(int maxLimit);
    }
}
