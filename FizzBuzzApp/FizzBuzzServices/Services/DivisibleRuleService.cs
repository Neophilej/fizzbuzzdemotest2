﻿using FizzBuzzRule.Interfaces;
using FizzBuzzServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzServices.Services
{
    public class DivisibleRuleService : IDivisibleRuleService
    {
        //List of rules which implements IDivisibleRule interface
        private readonly IEnumerable<IDivisibleRule> _divisibleRuleList;

        public DivisibleRuleService(IEnumerable<IDivisibleRule> divisibleRuleList)
        {
            _divisibleRuleList = divisibleRuleList;
        }

        /// <summary>
        /// Function to get all rules which returned true for the given input
        /// </summary>
        /// <param name="number">input number</param>
        /// <returns>Enumerable of rules which returned true for the given input</returns>
        public IEnumerable<IDivisibleRule> GetMatchedRules(int number)
        {
            return _divisibleRuleList.Where(rule => rule.Run(number,DateTime.UtcNow.DayOfWeek));
        }
    }
}
