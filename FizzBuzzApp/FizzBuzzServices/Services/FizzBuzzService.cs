﻿using FizzBuzzRule.Interfaces;
using FizzBuzzServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FizzBuzzServices.Services
{
    public class FizzBuzzService : IFizzBuzzService
    {
        //DivisibleRule Service
        private readonly IDivisibleRuleService _divisibleRuleServise;

        public FizzBuzzService(IDivisibleRuleService divisibleRuleServise)
        {
            _divisibleRuleServise = divisibleRuleServise;
        }

        /// <summary>
        ///  Method to get fizz buzz list
        /// </summary>
        /// <param name="maxLimit">input number</param>
        /// <returns>Fizz buzz list</returns>
        public IEnumerable<string> GetFizzBuzzList(int maxLimit)
        {
            var fizzBuzzList = new List<string>();
            for (var itrator = 1; itrator <= maxLimit; itrator++)
            {
                var ruleList = _divisibleRuleServise.GetMatchedRules(itrator);
                if (ruleList.Any())
                {
                    var resultString = string.Join(" ", ruleList.Select(rule => rule.Result));
                    fizzBuzzList.Add(resultString.Trim());
                }
                else
                {
                    fizzBuzzList.Add(itrator.ToString());
                }
            }
            return fizzBuzzList;
        }
    }
}
