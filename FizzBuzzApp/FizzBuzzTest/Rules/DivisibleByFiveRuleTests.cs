﻿using FizzBuzzRule;
using FizzBuzzRule.Interfaces;
using FizzBuzzRule.Rules;
using NUnit.Framework;
using System;

namespace FizzBuzzTest.Rules
{
    [TestFixture]
    public class DivisibleByFiveRuleTests
    {
        private IDivisibleRule _divisiblebyFiveRule;

        [SetUp]
        public void Setup()
        {
            this._divisiblebyFiveRule = new DivisibleByFiveRule();
        }

        [TestCase]
        public void When_Number_DivisibleByFive_ShouldReturn_Buzz_AndRuleShouldPass()
        {
            //act
            var result = this._divisiblebyFiveRule.Run(5, DayOfWeek.Monday);
            var ruleResult = this._divisiblebyFiveRule.Result;

            //Assert
            Assert.True(result);
            Assert.AreEqual(Constants.Buzz, ruleResult);
        }

        [TestCase]
        public void When_Number_Not_DivisibleByFive_ShouldReturn_EmptyString_AndRuleShouldFail()
        {
            //act
            var result = this._divisiblebyFiveRule.Run(4 ,DayOfWeek.Monday);
            var ruleResult = this._divisiblebyFiveRule.Result;

            //Assert
            Assert.False(result);
            Assert.AreEqual(string.Empty, ruleResult);
        }

        [TestCase]
        public void When_Number_DivisibleByFive_And_Its_Wednesday_ShouldReturn_wuzz_AndRuleShouldPass()
        {
            //act
            var result = this._divisiblebyFiveRule.Run(5, DayOfWeek.Wednesday);
            var ruleResult = this._divisiblebyFiveRule.Result;

            //Assert
            Assert.True(result);
            Assert.AreEqual(Constants.Wuzz, ruleResult);
        }
    }
}
