﻿using FizzBuzzRule;
using FizzBuzzRule.Interfaces;
using FizzBuzzRule.Rules;
using NUnit.Framework;
using System;

namespace FizzBuzzTest.Rules
{
    [TestFixture]
    public class DivisibleByThreeRuleTests
    {
        private IDivisibleRule _divisiblebyThreeRule;

        [SetUp]
        public void Setup()
        {
            this._divisiblebyThreeRule = new DivisibleByThreeRule();
        }

        [TestCase]
        public void When_Number_DivisibleByThree_ShouldReturn_Fizz_AndRuleShouldPass()
        {
            //act
            var result = this._divisiblebyThreeRule.Run(3, DayOfWeek.Monday);
            var ruleResult = this._divisiblebyThreeRule.Result;

            //Assert
            Assert.True(result);
            Assert.AreEqual(Constants.Fizz, ruleResult);
        }

        [TestCase]
        public void When_Number_Not_DivisibleByThree_ShouldReturn_EmptyString_AndRuleShouldFail()
        {
            //act
            var result = this._divisiblebyThreeRule.Run(4, DayOfWeek.Monday);
            var ruleResult = this._divisiblebyThreeRule.Result;

            //Assert
            Assert.False(result);
            Assert.AreEqual(string.Empty, ruleResult);
        }

        [TestCase]
        public void When_Number_DivisibleByThree_And_Its_WednesDay_ShouldReturn_Wizz_AndRuleShouldPass()
        {
            //act
            var result = this._divisiblebyThreeRule.Run(3, DayOfWeek.Wednesday);
            var ruleResult = this._divisiblebyThreeRule.Result;

            //Assert
            Assert.True(result);
            Assert.AreEqual(Constants.Wizz, ruleResult);
        }
    }
}
