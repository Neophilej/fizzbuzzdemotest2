﻿using FizzBuzzRule.Interfaces;
using FizzBuzzServices.Interfaces;
using FizzBuzzServices.Services;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System;
namespace FizzBuzzTest.Services
{
    [TestFixture]
    public class DivisibleRuleServiceTests
    {
        private IList<IDivisibleRule> divisibleRuleList;
        private Mock<IDivisibleRule> divisibleRuleMock;
        private IDivisibleRuleService divisibleRuleService;

        [TestFixtureSetUp]
        public void SetUp()
        {
            this.divisibleRuleList = new List<IDivisibleRule>();

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0),It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 5 == 0), It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleService = new DivisibleRuleService(this.divisibleRuleList);

        }

        [TestCase]
        public void When_Number_DivisibleByThree_ShouldReturn_OneRule()
        {
            //act
            var resultRules = this.divisibleRuleService.GetMatchedRules(3);
            //Assert

            Assert.AreEqual(1, resultRules.Count());
        }

        [TestCase]
        public void When_Number_DivisibleByFive_ShouldReturn_OneRule()
        {
            //act
            var resultRules = this.divisibleRuleService.GetMatchedRules(5);
            //Assert

            Assert.AreEqual(1, resultRules.Count());
        }

        [TestCase]
        public void When_Number_DivisibleByThreeAndFive_ShouldReturn_TwoRule()
        {
            //act
            var resultRules = this.divisibleRuleService.GetMatchedRules(15);
            //Assert

            Assert.AreEqual(2, resultRules.Count());
        }

        [TestCase]
        public void When_Number_Not_DivisibleByThreeOrFive_ShouldReturn_ZeroRule()
        {
            //act
            var resultRules = this.divisibleRuleService.GetMatchedRules(7);
            //Assert

            Assert.AreEqual(0, resultRules.Count());
        }

    }
}
