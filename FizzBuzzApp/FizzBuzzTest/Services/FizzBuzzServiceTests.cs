﻿using FizzBuzzRule;
using FizzBuzzRule.Interfaces;
using FizzBuzzServices.Interfaces;
using FizzBuzzServices.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
namespace FizzBuzzTest.Services
{
    [TestFixture]
    public class FizzBuzzServiceTests
    {
        private IList<IDivisibleRule> divisibleRuleList;
        private Mock<IDivisibleRule> divisibleRuleMock;
        private Mock<IDivisibleRuleService> divisibleRuleServiceMock;
        private IFizzBuzzService fizzbuzzService;

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleServiceMock = new Mock<IDivisibleRuleService>();
            this.fizzbuzzService = new FizzBuzzService(this.divisibleRuleServiceMock.Object);

        }

        [SetUp]
        public void SetUp()
        {
            this.divisibleRuleList = new List<IDivisibleRule>();
        }

        [TestCase]
        public void When_Number_DivisibleByThree_ShouldReturn_Fizz_InResultList()
        {
            // Arrange
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0),It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.Fizz);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(3);

            //Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "fizz" };
            CollectionAssert.AreEqual(expectedresult, result);
        }
        [TestCase]
        public void When_Number_DivisibleByFive_ShouldReturn_Buzz_InResultList()
        {
            // Arrange
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0), It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.Fizz);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 5 == 0), It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleMock.SetupGet(i => i.Result).Returns(Constants.Buzz);
            var divisibleRuleListForFive = new List<IDivisibleRule>() { divisibleRuleMock.Object };

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);
            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 5 == 0))).Returns(divisibleRuleListForFive);

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(5);

            //Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "fizz", "4", "buzz" };
            CollectionAssert.AreEqual(expectedresult, result);
        }
        [TestCase]
        public void When_Number_DivisibleByThreeAndFive_ShouldReturn_Fizz_Buzz_InResultList()
        {
            // Arrange

            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0), It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.Fizz);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 5 == 0), It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleMock.SetupGet(i => i.Result).Returns(Constants.Buzz);
            var divisibleRuleListForFive = new List<IDivisibleRule>() { divisibleRuleMock.Object };

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);
            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 5 == 0))).Returns(divisibleRuleListForFive);
            this.divisibleRuleServiceMock.Setup(d => d.GetMatchedRules(It.Is<int>(t => (t % 3 == 0 && t % 5 == 0)))).Returns(this.divisibleRuleList.Concat(divisibleRuleListForFive).AsEnumerable());

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(15);

            //Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "fizz", "4", "buzz", "fizz", "7", "8", 
                "fizz", "buzz", "11", "fizz",
                "13", "14", "fizz buzz"};
            CollectionAssert.AreEqual(expectedresult, result);
        }


        //WednesDayChecks

        [TestCase]
        public void When_Number_DivisibleByThree_WednesDay_ShouldReturn_Wizz_InResultList()
        {
            // Arrange
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0), It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.Wizz);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(3);

            //Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "wizz" };
            CollectionAssert.AreEqual(expectedresult, result);
        }

        [TestCase]
        public void When_Number_DivisibleByFive_WednesDay_ShouldReturn_Wuzz_InResultList()
        {
            // Arrange
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0), It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.Wizz);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 5 == 0), It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleMock.SetupGet(i => i.Result).Returns(Constants.Wuzz);
            var divisibleRuleListForFive = new List<IDivisibleRule>() { divisibleRuleMock.Object };

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);
            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 5 == 0))).Returns(divisibleRuleListForFive);

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(5);

            //Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "wizz", "4", "wuzz" };
            CollectionAssert.AreEqual(expectedresult, result);
        }

        [TestCase]
        public void When_Number_DivisibleByThreeAndFive_WednesDay_ShouldReturn_Wizz_Wuzz_InResultList()
        {
            // Arrange
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0), It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.Wizz);
            this.divisibleRuleList.Add(divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 5 == 0), It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleMock.SetupGet(i => i.Result).Returns(Constants.Wuzz);
            var divisibleRuleListForFive = new List<IDivisibleRule>() { divisibleRuleMock.Object };

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);
            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 5 == 0))).Returns(divisibleRuleListForFive);
            this.divisibleRuleServiceMock.Setup(d => d.GetMatchedRules(It.Is<int>(t => (t % 3 == 0 && t % 5 == 0)))).Returns(this.divisibleRuleList.Concat(divisibleRuleListForFive).AsEnumerable());

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(15);

            //Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "wizz", "4", "wuzz", "wizz", "7", "8", 
                "wizz", "wuzz", "11", "wizz",
                "13", "14", "wizz wuzz"};
            CollectionAssert.AreEqual(expectedresult, result);
        }
    }
}
